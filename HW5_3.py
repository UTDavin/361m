import google.cloud.dataflow as np
class My_SGD_Algorithm:  
    # Used for Matrix factorization X = UV^T
    
    # Will only use an error of sum of squares
    def __init__(self, learning_rate = .0001):
        # Initialization here
        self.__learning_rate = learning_rate
        
    # Input: X = matrix of m x n dimensions, r = rank
    # returns UV^T estimate of X
    def fit(self,X,r):  
        # Fit function here
        self.__U = np.matrix(1.5 * (1 + np.random.randn(X.shape[0],r)))  # U = m*r matrix of values in range (0,3)
        self.__V = np.matrix(1.5 * (1 + np.random.randn(X.shape[1],r)))  # V = n*r matrix of values in range (0,3)
        self.__V = np.transpose(self.__V) # r*n transposed matrix of V      
        orig_RMSE = self.score(X)
        self.__update__(X)
        new_RMSE = self.score(X)
        #update until RMSE stops improving
        while (new_RMSE < orig_RMSE):
            print new_RMSE
            iterations += 1
            orig_RMSE = new_RMSE
            self.__update__(X)
            new_RMSE = self.score(X)
            improve = orig_RMSE - new_RMSE
        est_X = self.__U * self.__V
        #fill estimator w/ nan's where X[i,j] is nan
        for i in range(0, self.__U.shape[0]):
            for j in range(0, self.__V.shape[1]):
                if(np.isnan(X[i,j])):
                    est_X[i,j] = np.nan
        return est_X           
        
    def set_learning_rate(self, learning_rate):
        self.__learning_rate = learning_rate
    
    #Returns RMSE of model vs input matrix X
    def score(self, X):
        est_X = self.__U * self.__V
        totalValues = 0
        SSE = 0
        for i in range(X.shape[0]):
            for j in range(X.shape[1]):
                if (np.isnan(X[i,j]) == False):
                    totalValues += 1
                    SSE += np.square(X[i,j] - est_X[i,j])
        if (totalValues == 0):
            return 0
        MSE = SSE/totalValues
        return np.sqrt(MSE) #RMSE
    # Private utility functions 
    
    # Utility function for fitting
    # Ui = Ui - learning_rate * dL_i,j(U,V)/dU_i
    # Vj = Vj - learning rate * dL_i,j(U,V)/dV_j
    # dL_i,j(U,V)/dUi = -2 * (X_i,j - sum_r[U_i,r * V_j,r])V_j
    # dL_i,j(U,V)/dVj = -2 * (X_i,j - sum_r[U_i,r * V_j,r])U_i
    def __update__(self, X):
        for i in range(0, self.__U.shape[0]):
            for j in range(0, self.__V.shape[1]):
                if (np.isnan(X[i,j]) == False):
                    Ui = self.__U[i,:]
                    Vj = self.__V[:,j]
                    Ui = Ui - self.__learning_rate * (-2 * (X[i,j] - float(Ui * Vj))) * np.transpose(Vj)
                    Vj = Vj - self.__learning_rate * (-2 * (X[i,j] - float(Ui * Vj))) * np.transpose(Ui)
                    self.__U[i,:] = Ui
                    self.__V[:,j] = Vj
                   
test_X = np.load('lens1m_361M_test.npy')
train_X = np.load('lens1m_361M_train.npy')
sgd = My_SGD_Algorithm()
est_X = sgd.fit(train_X,2) #2-rank matrix factorization
print "train RMSE: " + str(sgd.score(train_X))
print "test RMSE: " + str(sgd.score(test_X))
